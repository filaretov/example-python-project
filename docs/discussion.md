# Discussion

The design of this project was very carefully and deliberately chosen. Words
alone are not enough to describe it, the willing contributor is kindly
requested to study the source code, even if it seems a bit arcane at times.
