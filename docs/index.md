# Welcome to Example

This is an example Python project. You can literally do nothing with it.
Unless you're really into printing "Hello, World!", then this is totally what
you're looking for.

## CLI Usage

You can print the canonical message like this:

```
python -m example
```
