from invoke import task

POETRY_RUN = "poetry run"
POETRY_VERSION = "$(poetry version -s)"


def get_version(version: str) -> str:
    if version == "poetry":
        version = POETRY_VERSION
    return version


@task
def build(c):
    c.run("poetry build")


@task
def serve(c):
    c.run(f"{POETRY_RUN} mkdocs serve")


@task
def docs_deploy(c, version="poetry"):
    """
    Deploy the docs with a version spec.

    Args:
      version: Either a valid version specifier or 'poetry'. If
            'poetry', the poetry executable is used to read the version.
    """
    version = get_version(version)
    c.run(f"{POETRY_RUN} mike deploy {POETRY_VERSION} --push")


@task
def docs_update_latest(c, version="poetry"):
    """
    Update the latest alias of the docs.

    Args:
      version: Either a valid version specifier or 'poetry'. If
            'poetry', the poetry executable is used to read the version.
    """
    version = get_version(version)
    c.run(f"{POETRY_RUN} mike deploy --push --update-aliases {version} latest")

@task
def test(c):
    c.run(f"{POETRY_RUN} pytest")
